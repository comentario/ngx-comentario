# ![](icon.svg) ngx-comentario

This project contains the [ngx-comentario](https://www.npmjs.com/package/ngx-comentario) Angular library that allows to embed comments served by [Comentario](https://gitlab.com/comentario/comentario) into an existing Angular application.

## Demo Application

This repository also contains a demo app, which you can run to see the above library in action.

1. Either [configure](https://docs.comentario.app/en/getting-started/) a Comentario server to run locally or use an existing Comentario instance (update `scriptUrl` in the file `projects/demo-app/src/app/app.component.ts` accordingly).
2. Add a new domain in Comentario's Administration UI with `http://localhost:4200` for host.
3. Start the demo application: `npm start`.
4. Navigate to [localhost:4200](http://localhost:4200) in your browser to see the app's homepage.
5. Try switching between **Home**, **Page A**, and **Page B**: each page must have its own comment thread.
