# ngx-comentario changelog

## v0.6.0

* upgrade to Angular 19 - f7da9c1
* supported Angular version range is now [12...19] - f7da9c1

## v0.5.0

* get rid of `extraOptions` and router subscription - ce2697e
* get rid of `@angular/router` dependency - ce2697e
* add `pageId` property - ce2697e
* bump Angular deps - ce2697e

## v0.4.0

* add `extraOptions` property, allowing to ignore query params - 8de3374
* add console warning when script not set (dev mode only) - 8de3374

## v0.3.0

* replace comentarioUrl with optional scriptUrl allow Angular >=12.0.0 - 8bc42c5
* add icon, add automated CI pipeline - 2fb2f15, 2815a60, 1693e3b, 1d947d4, 8d47bcc, 6b76613, 23f301f, 0d17fab, b2944be, cc85b76, 5f9524c, dac0909, a39f2e0, 6886d84

## v0.2.0

* add demo-app - 8f31de9
 
## v0.1.3

* fix re-adding existing script - 2325b11

* ## v0.1.2

First public release - 47afd2e, 2aab305, 991729c
