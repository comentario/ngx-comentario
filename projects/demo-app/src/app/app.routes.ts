import { Routes } from '@angular/router';
import { PageAComponent } from './page-a/page-a.component';
import { PageBComponent } from './page-b/page-b.component';

export const routes: Routes = [
    {path: 'a', component: PageAComponent},
    {path: 'b', component: PageBComponent},
];
