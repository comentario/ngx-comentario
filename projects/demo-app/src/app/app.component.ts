import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationEnd, Router, RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgxComentarioComponent } from '../../../ngx-comentario/src/lib/ngx-comentario.component';

@Component({
    selector: 'app-root',
    standalone: true,
    imports: [CommonModule, RouterOutlet, NgxComentarioComponent, RouterLink, RouterLinkActive],
    templateUrl: './app.component.html',
})
export class AppComponent implements OnDestroy {

    pageId?: string;

    readonly scriptUrl = 'http://localhost:8080/comentario.js';

    private routerSub: Subscription;

    constructor(
        router: Router,
    ) {
        this.routerSub = router.events.subscribe(e => e instanceof NavigationEnd && (this.pageId = e.urlAfterRedirects));
    }

    ngOnDestroy(): void {
        this.routerSub.unsubscribe();
    }
}
