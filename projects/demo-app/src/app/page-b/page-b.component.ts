import { Component } from '@angular/core';

@Component({
  selector: 'app-page-b',
  standalone: true,
  imports: [],
  template: '<p>This is the Page B!</p>',
})
export class PageBComponent {}
