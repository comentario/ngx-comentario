import { Component } from '@angular/core';

@Component({
  selector: 'app-page-a',
  standalone: true,
  imports: [],
  template: '<p>This is the Page A!</p>',
})
export class PageAComponent {}
