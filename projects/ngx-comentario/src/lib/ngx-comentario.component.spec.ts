import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxComentarioComponent } from './ngx-comentario.component';

describe('NgxComentarioComponent', () => {

    let component: NgxComentarioComponent;
    let fixture: ComponentFixture<NgxComentarioComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [NgxComentarioComponent],
        })
            .compileComponents();

        fixture = TestBed.createComponent(NgxComentarioComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('is created', () => {
        expect(component).toBeTruthy();
    });
});
