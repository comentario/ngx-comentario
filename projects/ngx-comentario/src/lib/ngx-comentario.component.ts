import { AfterViewInit, Component, ElementRef, Inject, Input, isDevMode, OnChanges, Renderer2, SimpleChanges } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'ngx-comentario-comments',
    standalone: true,
    imports: [],
    template: '',
})
export class NgxComentarioComponent implements AfterViewInit, OnChanges {

    /**
     * Optional URL of the Comentario script, looking like "https://example.com/comentario.js". If not provided, you'll
     * have to include it yourself, preferably in the document's <head>.
     */
    @Input()
    scriptUrl: string | null | undefined;

    /** Optional URL of the CSS override file, or `false` to disable applying any styles altogether. */
    @Input()
    cssOverride?: string | false;

    /** Optional maximum visual nesting level of comments (1 or more). */
    @Input()
    maxLevel?: number;

    /** Whether to disable applying standard fonts. */
    @Input()
    noFonts?: boolean;

    /**
     * Optional page ID to pass to the web component, which should map to a real page path. If not provided, Comentario
     * will use the path of the current window URL.
     */
    @Input()
    pageId?: string;

    private elScript?: HTMLScriptElement;
    private elComments?: HTMLElement;
    private scriptWarning = false;

    constructor(
        @Inject(DOCUMENT) private readonly doc: Document,
        private readonly renderer: Renderer2,
        private readonly el: ElementRef<HTMLElement>,
    ) {}

    ngAfterViewInit(): void {
        // Init the comments
        this.reinsertComponent();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['scriptUrl']) {
            this.reinsertScript();
        }
        if (this.elComments && ['cssOverride', 'maxLevel', 'noFonts', 'pageId'].some(k => k in changes)) {
            this.reinsertComponent();
        }
    }

    private reinsertScript() {
        // Remove the script element, if any
        if (this.elScript) {
            this.renderer.removeChild(this.doc.head, this.elScript);
            this.elScript = undefined;
        }

        // Check if there's a script URL to add
        if (!this.scriptUrl) {
            return;
        }

        // Make sure we're not re-adding the script added previously
        const scr = this.doc.querySelector(`script[src="${this.scriptUrl}"]`);
        if (scr) {
            this.elScript = scr as HTMLScriptElement;
            return;
        }

        // Insert and set up a script element
        this.elScript = this.renderer.createElement('script');
        this.renderer.setAttribute(this.elScript, 'src', this.scriptUrl);
        this.renderer.appendChild(this.doc.head, this.elScript);
    }

    private reinsertComponent() {
        // Perform registry check
        this.checkComponentRegistered();

        // Remove the comments web component, if any
        if (this.elComments) {
            this.renderer.removeChild(this.el.nativeElement, this.elComments);
            this.elComments = undefined;
        }

        // Create and set up the web component
        this.elComments = this.renderer.createElement('comentario-comments');
        if (this.cssOverride !== undefined) {
            this.renderer.setAttribute(this.elComments, 'css-override', this.cssOverride.toString());
        }
        if (this.maxLevel !== undefined && this.maxLevel > 0) {
            this.renderer.setAttribute(this.elComments, 'max-level', this.maxLevel.toString());
        }
        if (this.noFonts !== undefined) {
            this.renderer.setAttribute(this.elComments, 'no-fonts', this.noFonts.toString());
        }
        if (this.pageId) {
            this.renderer.setAttribute(this.elComments, 'page-id', this.pageId);
        }

        // Reinsert the comments web component into the DOM
        this.renderer.appendChild(this.el.nativeElement, this.elComments);
    }

    private checkComponentRegistered() {
        // Perform registry check when in dev mode, but only once
        if (isDevMode() && !this.scriptWarning && !this.scriptUrl && !customElements.get('comentario-comments')) {
            this.scriptWarning = true;
            console.warn(
                'NgxCommentsComponent: web component "comentario-comments" isn\'t registered. This usually means you ' +
                'forgot to set the scriptUrl property and didn\'t add a <script> tag yourself either.');
        }
    }
}
