# ngx-comentario

**ngx-comentario** is a library that allows to easily embed comments served by [Comentario](https://gitlab.com/comentario/comentario) into an existing Angular application.

## Supported Comentario versions

This library supports all Comentario versions as of **3.0.0**, although the support for properties may vary. See the property table below for details.

## Installation

```bash
npm install --save ngx-comentario
```

## Dependencies

ngx-comentario specifies the following components as *peer dependencies*, which means you have to have them installed for your application already (make sure they are listed under `dependencies` in your `package.json`):

* `@angular/core`
* `@angular/common`

## Usage

Add the `<ngx-comentario-comments>` tag to your application at the point you want your comments to appear, for example:

```html
<ngx-comentario-comments
    scriptUrl="https://comentario.example.com/comentario.js"></ngx-comentario-comments>
```

### Attributes

The behaviour of Comentario and the appearance of the comments can be customised by setting additional attributes on the `<ngx-comentario-comments>` tag, described below.

All attributes are optional, however, if you don't provide `scriptUrl`, you'll have to add the required `<script>` element to your page (preferably to its `<head>` element) yourself.

| Attribute     | Type              | Description                                                                                                        |
|---------------|-------------------|--------------------------------------------------------------------------------------------------------------------|
| `scriptUrl`   | `string`          | Complete URL of Comentario script.                                                                                 |
| `cssOverride` | `string \| false` | URL of an additional CSS stylesheet to load, or `false` to disable styling altogether.                             |
| `maxLevel`    | `number`          | Maximum visual comment nesting level (`1` or more). (Comentario 3.1.0+)                                            |
| `noFonts`     | `boolean`         | If set to `true`, no standard fonts will be applied to the comments.                                               |
| `pageId`      | `string`          | Page ID, which maps to a real app path and starts with a `/`. If not provided, the current page path will be used. |

### Single component for multiple routes

In a routed Angular application, it's quite often the case a single component serves multiple routes. This is typically achieved by adding route parameters to the config, for instance:

```typescript
@NgModule({
    imports: [RouterModule.forRoot([
        {path: 'blog/post/:id', component: BlogPostComponent},
    ])],
    exports: [RouterModule],
})
export class AppRoutingModule {}
```

In this example, the `BlogPostComponent` will display a post with the given ID, obtained from the `ActivatedRoute`.

If you insert an `<ngx-comentario-comments>` into the `BlogPostComponent`'s template, you'll also need to make sure the path it uses for comment display changes along with route changes.

The following example demonstrates how you can use a route parameter subscription to calculate a `pageId`, which is then bound to the same-named property in the template.

`blog-post-component.html`:
```html

<!-- Post title -->
<h1>{{ post.title }}</h1>

<!-- Post text -->
<div>{{ post.text }}</div>

<!-- Comments -->
<h2>Comments</h2>

<!-- Hide any comments until there's a pageId -->
<ng-container *ngIf="pageId">
    <ngx-comentario-comments
        scriptUrl="https://comentario.example.com/comentario.js"
        [pageId]="pageId"></ngx-comentario-comments>
</ng-container>
```

`blog-post-component.ts`:
```typescript
@Component({
    selector: 'app-blog-post',
    templateUrl: './blog-post-component.html',
})
export class BlogPostComponent {

    pageId?: string;

    constructor(route: ActivatedRoute) {
        route.paramMap.subscribe(pm => this.pageId = pm.has('id') ? `/blog/post/${pm.get('id')}` : undefined);
    }
}
```

See also the `demo-app` project for a slightly different approach that uses a `Router` subscription (because comments are rendered for child routes of the `AppComponent`).
